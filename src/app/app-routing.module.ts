import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'special', loadChildren: './pages/special/special.module#SpecialPageModule' },
  { path: 'success', loadChildren: './pages/success/success.module#SuccessPageModule' },
  { path: 'personal', loadChildren: './pages/personal/personal.module#PersonalPageModule' },
  { path: 'regeneration', loadChildren: './pages/regeneration/regeneration.module#RegenerationPageModule' },
  { path: 'community', loadChildren: './pages/community/community.module#CommunityPageModule' },
  { path: 'calculator', loadChildren: './pages/calculator/calculator.module#CalculatorPageModule' },
  { path: 'carestep', loadChildren: './pages/carestep/carestep.module#CarestepPageModule' },
  { path: 'personal-rehabil', loadChildren: './pages/personal-rehabil/personal-rehabil.module#PersonalRehabilPageModule' },
  { path: 'commStory', loadChildren: './pages/comm-story/comm-story.module#CommStoryPageModule' },
  { path: 'online-consult', loadChildren: './pages/online-consult/online-consult.module#OnlineConsultPageModule' },
  { path: 'review', loadChildren: './pages/review/review.module#ReviewPageModule' },
  { path: 'review-write', loadChildren: './pages/review-write/review-write.module#ReviewWritePageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
 