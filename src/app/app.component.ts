import { Component, ViewChildren, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;
//import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalController, MenuController, ActionSheetController, PopoverController, IonRouterOutlet, AlertController, } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    { title: '회사소개', url: '/home', icon: 'home' },
    { title: '특별한백양', url: '/special', icon: 'sunny' },
    { title: '성공사례', url: '/success', icon: 'albums' },
    { title: '개인회생', url: '/personal', icon: 'partly-sunny' },
    { title: '회생가이드', url: '/regeneration', icon: 'refresh' },
    { title: '회생스토리', url: '/community', icon: 'folder-open' },
    { title: '고객후기', url: '/review', icon: 'people' },
  ];

  // set up hardware back button event.
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform,
    //private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public modalCtrl: ModalController,
    private menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    public alertCtrl: AlertController,) {

    this.initializeApp();
    // Initialize BackButton Eevent.
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      //this.splashScreen.hide();
      // Hide the splash (you should do this on app launch)
      SplashScreen.hide();
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
        // close action sheet
        try {
            const element = await this.actionSheetCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
        }

        // close popover
        try {
            const element = await this.popoverCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
        }

        // close modal
        try {
            const element = await this.modalCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
            console.log(error);

        }

        // close side menu
        try {
            const element = await this.menu.getOpen();
            if (element !== null) {
                this.menu.close();
                return;
            }
        } catch (error) {
        }

        this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
            if (outlet && outlet.canGoBack()) {
                outlet.pop();
            } else if(this.router.url === '/home'
                      /*
                      this.router.url === '/special' || 
                      this.router.url === '/success' || 
                      this.router.url === '/personal' || 
                      this.router.url === '/regeneration' || 
                      this.router.url === '/community' || 
                      this.router.url === '/calculator'
                      */
                      ) {
                        navigator['app'].exitApp(); // work for ionic 4

            }
        });

    });
  }

  navigate(url: string) {
    return this.router.navigateByUrl(url);
  }
}
