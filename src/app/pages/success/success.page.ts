import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ModalController, AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
import "rxjs/add/operator/map";

declare var window;

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";

  top_image: any = this.web_image_url+"sub_visual02.jpg";

  initParam: any;
  courtParam: any;
  court: any;
  courts: any = [
    { label: "전체", value: "전체" },
    { label: "서울회생법원", value: "서울회생법원" },
    { label: "의정부지법", value: "의정부지법" },
    { label: "수원지법", value: "수원지법" },
    { label: "인천지법", value: "인천지법" },
    { label: "춘천지법", value: "춘천지법" },
    { label: "강릉지법", value: "강릉지법" },
    { label: "청주지법", value: "청주지법" },
    { label: "대전지법", value: "대전지법" },
    { label: "대구지법", value: "대구지법" },
    { label: "부산지법", value: "부산지법" },
    { label: "울산지법", value: "울산지법" },
    { label: "창원지법", value: "창원지법" },
    { label: "전주지법", value: "전주지법" },
    { label: "광주지법", value: "광주지법" },
    { label: "제주지법", value: "제주지법" },
  ];

  decisionParam: any;
  decision: any;
  decisions: any = [
    { label: "전체", value: "전체" },
    { label: "신청접수", value: "신청접수" },
    { label: "개시결정", value: "개시결정" },
    { label: "금지명령", value: "금지명령" },
  ]
  successCases: any = [];
  last_wr_id: any;
  doInfiniteFlag: boolean = true;

  constructor(private http: Http,
              private router: Router,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,) { }

  ngOnInit() {
    this.courtParam = '&court=전체';
    this.decisionParam = '&decision=전체';
    this.initParam = 'last_wr_id=0';
    this.getSuccessCase();
  }

  getSuccessCase() {
    this.successCases = [];
    let queryParams =  this.initParam + this.courtParam + this.decisionParam;
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.base_url + "by_getSuccessCase.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { // 'result'=>true, 'successCase'=>$o
      for (let i = 0; i < Object.keys(data.successCase).length; i++) {
        let text_length = data.successCase[i].subject.length;
        let remove_position = data.successCase[i].subject.lastIndexOf( "]" ) +1;
        let remove_length = text_length - remove_position;
        data.successCase[i].subject = data.successCase[i].subject.substr(remove_position, remove_length);

        data.successCase[i].content = data.successCase[i].content.replace(/<\/?[^>]+(>|$)/g, "");
        text_length = data.successCase[i].content.length;
        remove_position = data.successCase[i].content.lastIndexOf( "문의" ) +2;
        remove_length = text_length - remove_position;
        data.successCase[i].content = data.successCase[i].content.substr(0, remove_length);

        this.successCases.push(data.successCase[i]);
      }
      this.last_wr_id = data.last_wr_id;
      console.log('successCase last_wr_id -----> '+this.last_wr_id);
    });
  }

  selectedCourt(e) {
    console.log('selected Court --------> ' +this.court);
    this.courtParam = '&court=' +this.court;
    this.getSuccessCase();
  }

  selecteddecision(e) {
    console.log('selected decision --------> ' +this.decision);
    this.decisionParam = '&decision=' +this.decision;
    this.getSuccessCase();
  }

  /* Alert  */
  async openSuccessStory(successCase) {
    const alert = await this.alertCtrl.create({
      header: successCase.decision,
      subHeader: successCase.subject,
      message: successCase.content,
      buttons: ['닫기']
    });
    await alert.present();
  }

  
  /* Modal 
  async openSuccessStory(successCase) {
    const modal = await this.modalCtrl.create({
      component: SuccessStoryPage,
      componentProps: { passedParam: successCase }
    });
    return await modal.present();
  }
  */

  loadData(event) {
    if(this.doInfiniteFlag == true) {
      let infiniteParam =  'last_wr_id='+this.last_wr_id;
      let queryParams =  infiniteParam + this.courtParam + this.decisionParam;
      let body   : string	 = queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.base_url + "by_getSuccessCase.php";

      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != true) {
          this.doInfiniteFlag = false;
        } else {
          for (let i = 0; i < Object.keys(data.successCase).length; i++) {
            let text_length = data.successCase[i].subject.length;
            let remove_position = data.successCase[i].subject.lastIndexOf( "]" ) +2;
            let remove_length = text_length - remove_position;
            data.successCase[i].subject = data.successCase[i].subject.substr(remove_position, remove_length);
            this.successCases.push(data.successCase[i]);
          }
          this.last_wr_id = data.last_wr_id;
          console.log('successCase last_wr_id -----> '+this.last_wr_id);
          if( Object.keys(data.successCase).length < 25) {
            this.doInfiniteFlag = false;
          }
        }
        // console.log('Next operation has ended');
        event.target.complete();
      });
    }
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
    
  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
