import { Component, ViewChild, ElementRef  } from '@angular/core';
import { Router } from '@angular/router';
import { Content, AlertController, } from '@ionic/angular';

declare var google: any;
declare var Swiper: any;
declare var window;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild(Content) content: Content;
  @ViewChild('mapCanvas') mapElement: ElementRef;

  mapData: any = [{
    "name": "백양법무사 김형백사무소",
    "address": "서울시 양천구 신월로 374, 503호 (신정동, 도림빌딩)",
    "tel": "Tel. 1566-2320",
    "lat": 37.5219277,
    "lng": 126.8625060,
    "center": true
  }]

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"

  parallax_bg_img: any = this.mobile_image_url+"main_1.jpg";
  images: any = [
    { img: this.mobile_image_url+"main4.png"},
    { img: this.mobile_image_url+"main5.jpg"},
    { img: this.mobile_image_url+"main6.jpg"},
  ]
  promise_bg: any = this.web_image_url+"company_002_bg.gif";
  promises: any = [
    { img: this.web_image_url+"promise_img_01.png",
      color: "light",
      title: "약속 하나,",
      subtitle: "1:1고객맞춤 무료상담 서비스제공", 
      description: "개인회생은 의뢰인의 부채상황,부양가족 등 여러가지 상황에 따라서 사건을 진행하는 방향이 다릅니다. 백양은 첫 상담부터 무료상담을 진행하며 항상 고객의 입장에서 친절하고 정확한 상담서비스를 제공합니다."
    },
    { img: this.web_image_url+"promise_img_02.png",
      color: "light",
      title: "약속 둘,",
      subtitle: "전국 상담 및 무방문 접수 가능",
      description: "직접 방문상담이 어려우신 고객의 상황에 따라 맞춤 상담을 진행하며, 전국 무방문으로 간편접수가 가능합니다."
    },
    { img: this.web_image_url+"promise_img_03.png",
      color: "light",
      title: "약속 셋,",
      subtitle: "회생/파산 비용 분납 가능", 
      description: "회생/파산을 진행하시는 분들은 아무래도 수임료가 부담이 될 수 밖에 없습니다. 백양에서는 비용을 최대 4회차까지 분납이 가능하며 의뢰인분의 부담을 최대한 덜어 드리고자 합니다."
    },
    { img: this.web_image_url+"promise_img_04.png",
      color: "light",
      title: "약속 넷,",
      subtitle: "각 분야별 담당업무의 체계적인 분업", 
      description: "한 직원이 여러 업무를 진행하면 효율성과 정확성 면에서 뒤떨어질 수 밖에 없습니다. 백양법무사 김형백사무소에서는 각 분야별 최고의 전문가들이 업무를 분업화하여 모든 과정을 신속하고 정확하게 처리하는 백양입니다."
    },
    { img: this.web_image_url+"promise_img_05.png",
      color: "light",    
      title: "약속 다섯,", 
      subtitle: "철저한 사후관리 서비스 제공", 
      description: "개인회생 인가결정은 끝이 아니라 시작점입니다. 백양에서는 개시일정 혹은 인가 결정 이후에도 의뢰인 하나하나 세심하게 관리해 드리고 있으며 문제가 발생 할 시 최선의 솔루션을 제공해 드리고 있습니다."
    },
    { img: this.web_image_url+"promise_img_06.png",
      color: "light",
      title: "채무해방의 꿈,",
      subtitle: "백양이 함께하겠습니다.", 
      description: "모두 채무에서 벗어나 행복한 삶을 그리는 고객을 위해 백양은 놀라울 정도로 세분화된 법률컨설팅을 제공하는데 큰 자부심을 느낍니다. 백양 법무소는 경쟁력을 강화하고 고객들이 보여주신 신뢰를 이어나가기 위해 지금까지 이룬 성과를 바탕으로 새로 능력과 전문성을 구축할 것 입니다."
    },
  ]

  backYangFlag: boolean = false;
  privacyPolicyContent: any = 
  "<p>백양법무사는 개인정보 보호법률(정보통신망법)의 개인정보 취급방침 을 준수합니다.</p><p>[개인정보 취급방침]</p><p>▣ 개인정보 수집 및 이용목적</p><p>- 신청내역 확인(서비스 이용에 따른 본인식별, 본인의사 확인, 불만처리 등 의사소통 경로 확보).</p><p>- 개인별 맞춤서비스 제공을 위한 정보 확인.</p><p>- 기타 양질의 서비스 제공을 위한 정보 확인.<p><p>※ 신용카드 정보는 오직 결제에 대한 수단으로 사용되며, 카드 정보는  일체 수집하지 않음.</p><p>▣ 수집하는 개인정보 항목</p><p>- 부모이름(본인이름), 인적사항, 생년월일, 이메일, 팩스, 주소, 전화번호, 핸드폰번호, 기타 선택항목</p><p>▣ 개인정보 보유 및 이용기간</p><p>- 개인정보는 개인별 서비스 제공을 위하여 일정기간 보유하며, 서비스 제공이 만료되었다고 판단되면 즉시 파기합니다.</p><p>단, 전자상거래법 소비자보호에 관한 법률 등 타 법률에 의해 보존할 필요성이 있는 경우에는 일정기간 보존합니다.</p><p>가. 신청서에 관한 기록: 3년</p><p>나. 대금결제 및 재화 등의 공급에 관한 기록: 3년</p><p>다. 소비자불만 또는 분쟁처리에 관한 기록: 3년</p>";

  constructor(
    private router: Router,
    public alertCtrl: AlertController,){
  }

  ngOnInit(){
    this.loadMap();
  }

  ionViewDidEnter() {
    var swiper1 = new Swiper('.swiper1', {
      pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    var swiper2 = new Swiper('.swiper2' , {
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination2',
        dynamicBullets: true,
      },
    });
  }

loadMap(){
  const mapEle = this.mapElement.nativeElement;

  const map = new google.maps.Map(mapEle, {
    center: this.mapData.find((d: any) => d.center),
    zoom: 16
  });

  this.mapData.forEach((markerData: any) => {
    const infoWindow = new google.maps.InfoWindow({
      content: `<h5>${markerData.name}</h5><h6>${markerData.tel}</h6><h6>${markerData.address}</h6>`
    });

    const marker = new google.maps.Marker({
      position: markerData,
      map,
      animation: google.maps.Animation.DROP,
      title: markerData.name
    });

    marker.addListener('click', () => {
      infoWindow.open(map, marker);
    });
  });

  google.maps.event.addListenerOnce(map, 'idle', () => {
    mapEle.classList.add('show-map');
  });
}

  backyang() {
    this.backYangFlag = !this.backYangFlag;
    this.scrollToBottom();
  }

  async privacyPolicy() {
    const alert = await this.alertCtrl.create({
      header: '개인정보 취급방침',
      message: this.privacyPolicyContent,
      buttons: ['닫기']
    });
    await alert.present();
  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    });
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  
  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
