import { Component, OnInit, ViewChild,  } from '@angular/core';
import { Content, AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
import numeral from 'numeral';

declare var window;

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss'],
})
export class CalculatorPage implements OnInit {

  @ViewChild(Content) content: Content;

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"

  top_image: any = this.web_image_url+"sub_visual03.jpg";

  avg_monthly_salary: any;   // 월평균소득
  total_debt: any;           // 신용채무총액
  dependents_number: any;    // 부양 가족수
  property: any;             // 재     산

  avg_monthly_salary_formatted: any;   // 월평균소득
  total_debt_formatted: any;           // 신용채무총액
  dependents_number_formatted: any;    // 부양 가족수
  property_formatted: any;             // 재     산

  exp_monthly_payment: any;  // 예상 변제금(월)
  payment_months: any;       // 변제 개월수
  total_payment: any;        // 총 변제금
  exempt_money: any;         // 원금 탕감액
  message: any;

  dependents_table: any = [
    { dependents: 0, living_cost: 0 },
    { dependents: 1, living_cost: 1003263 },
    { dependents: 2, living_cost: 1708258 },
    { dependents: 3, living_cost: 2209890 },
    { dependents: 4, living_cost: 2711521 },
    { dependents: 5, living_cost: 3113152 },
    { dependents: 6, living_cost: 3714784 },
  ]

  constructor(
    public alertCtrl: AlertController,
    private router: Router,) { 
      
    }

  ngOnInit() {
  }

  onChangeSalary(evt) {
    //console.log('onChangeSalary event');
    this.avg_monthly_salary_formatted = evt.replace(/[^0-9.]/g, "");
    if (this.avg_monthly_salary_formatted) {
      let myNumeral = numeral(this.avg_monthly_salary_formatted);
      let value = myNumeral.value();
      this.avg_monthly_salary = Number(value);
      if(this.avg_monthly_salary > 0) {
        this.avg_monthly_salary_formatted = numeral(value).format('0,0');        
      } else {
        this.presentAlert('월 평균소득을 입력해주세요'); 
      }
    } else {
      //this.avg_monthly_salary_formatted = ''; 
      this.avg_monthly_salary = 0;
      //this.presentAlert('숫자만 입력할 수 있습니다.');
    }
  }

  onChangeDebt(evt) {
    this.total_debt_formatted = evt.replace(/[^0-9.]/g, "");
    if (this.total_debt_formatted) {
      let myNumeral = numeral(this.total_debt_formatted);
      let value = myNumeral.value();
      this.total_debt = Number(value);
      if(this.total_debt > 0) {
        this.total_debt_formatted = numeral(value).format('0,0');        
      } else {
        this.presentAlert('신용채무총액을 입력해주세요'); 
      }
    } else {
      //this.total_debt_formatted = ''; 
      this.total_debt = 0;
      //this.presentAlert('숫자만 입력할 수 있습니다.');
    }
  }

  onChangeDependents(evt) {
    this.dependents_number_formatted = evt.replace(/[^0-9.]/g, "");
    if (this.dependents_number_formatted) {
      let myNumeral = numeral(this.dependents_number_formatted);
      let value = myNumeral.value();
      this.dependents_number = Number(value);
      if(this.dependents_number > 0) {
        this.dependents_number_formatted = numeral(value).format('0,0');        
      } else {
        this.presentAlert('본인포함 부양 가족수를 입력해주세요'); 
      }
    } else {
      //this.dependents_number_formatted = ''; 
      this.dependents_number = 0;
      //this.presentAlert('숫자만 입력할 수 있습니다.');
    }
  }

  onChangeProperty(evt) {
    this.property_formatted = evt.replace(/[^0-9.]/g, "");
    if (this.property_formatted) {
      let myNumeral = numeral(this.property_formatted);
      let value = myNumeral.value();
      this.property = Number(value);
      if(this.property >= 0) {
        this.property_formatted = numeral(value).format('0,0');        
      } else {
        this.presentAlert('현 재산을 입력해주세요'); 
      }
    } else {
      //this.property_formatted = ''; 
      this.property = 0;
      //this.presentAlert('숫자만 입력할 수 있습니다.');
    }
  }
  onFocusProperty(ev) {
    this.scrollToBottom();
  }

  scrollToBottom() {
    //console.log('=====> scrollToBottom');
    setTimeout(() => {
      this.content.scrollToBottom();
    });
  }

  async presentAlert(contents) {
    const alert = await this.alertCtrl.create({
      header: '알림',
      subHeader: contents,
      buttons: ['확인']
    });
    await alert.present();
  }
  
  DoCalculator(){
    if(this.avg_monthly_salary_formatted == undefined || 
      this.total_debt_formatted == undefined || 
      this.dependents_number_formatted == undefined || 
      this.property_formatted == undefined) {
      this.presentAlert('월 평균소득, 신용채무총액, 부양가족수, 재산을 모두 입력하십시요');
      return;
    }

    let avg_monthly_salary = +this.avg_monthly_salary;
    let total_debt = +this.total_debt;
    let dependents_number = +this.dependents_number;
    let property = +this.property;

    //console.log('avg_monthly_salary: '+avg_monthly_salary);
    //console.log('total_debt: '+total_debt);
    //console.log('dependents_number: '+dependents_number);
    //console.log('property: '+property);

    let temp_dependents = 0;
    let temp_living_cost = 0;
    this.message = '';

    if (property >= total_debt) { // 재산 > 신용채무총액
      this.presentAlert('고객님의 현 재산이 신용채무총액 보다 같거나, 많으므로 회생 자격이 없습니다.');
      return;
    }
  
    if(avg_monthly_salary < 1200000) {
      this.presentAlert('월 평균소득은 120만원 이상 입력하십시오.');
      return;
    }
  
    for (let i = 0; i < this.dependents_table.length; i++) {
      if(this.dependents_table[i].dependents == dependents_number) {
        //console.log('부양가족 수: '+this.dependents_table[i].dependents);
        //console.log('최저 생계비: '+this.dependents_table[i].living_cost);
        temp_dependents = this.dependents_table[i].dependents;
        temp_living_cost = this.dependents_table[i].living_cost;
        this.message = '부양가족 수: '+temp_dependents+ '명,  최저 생계비: '+numeral(temp_living_cost).format('0,0')+'원';
        break;
      }
    }
    /*
    { dependents: 1, living_cost: 1003263 },
    { dependents: 2, living_cost: 1708258 },
    { dependents: 3, living_cost: 2209890 },
    { dependents: 4, living_cost: 2711521 },
    { dependents: 5, living_cost: 3113152 },
    { dependents: 6, living_cost: 3714784 },
    */
    if(avg_monthly_salary < temp_living_cost) {  // 월평균소득 < 부양생계비 1200000 < 1700000
      for (let i = 6; i > 0; i--) {
        //console.log('for (let i = 6; i > 0; i--) i ===> '+i);
        if(avg_monthly_salary > this.dependents_table[i].living_cost) { // 월평균소득 > 부양생계비
          temp_living_cost = this.dependents_table[i].living_cost;
          this.message = '부양가족 수: '+temp_dependents+ '명,  적용된 최저 생계비: '+numeral(temp_living_cost).format('0,0')+'원';
          break;
        }
      }
    }

    this.exp_monthly_payment = avg_monthly_salary - temp_living_cost;  // 월예상변제금 = 월평균소득 - 생계비
    console.log('예상 변제금(월): '+this.exp_monthly_payment);

    if(property < (this.exp_monthly_payment * 36)) 
    {
      console.log('변제 개월 수: 36개월');
      this.payment_months = 36;
      this.total_payment = this.exp_monthly_payment * this.payment_months; // 예상변제금총액 = 월예상변제금 * 36월(변제 개월수)
      if(this.total_payment > total_debt){
        this.payment_months = Math.ceil(total_debt / this.exp_monthly_payment);
        this.total_payment = this.exp_monthly_payment * this.payment_months;
        if(this.total_payment > total_debt){
          this.total_payment = total_debt;
        }
        console.log('수정 변제 개월 수: '+this.payment_months+'개월');
      }
      console.log('총 변제금: '+this.total_payment);
    } else if(((this.exp_monthly_payment * 36) <= property) && (property < (this.exp_monthly_payment * 60))) 
          {
            this.payment_months = Math.ceil(property / this.exp_monthly_payment);
            console.log('변제 개월 수: '+this.payment_months+'개월');
            this.exp_monthly_payment = Math.ceil(property / this.payment_months);  // 월예상변제금 = 재산 / 변제 개월수
            this.total_payment = this.exp_monthly_payment * this.payment_months; // 예상변제금총액 = 월예상변제금 * 변제 개월수
            console.log('총 변제금: '+this.total_payment);
          } else if(( property >= (this.exp_monthly_payment * 60)))
                {
                  console.log('변제 개월 수: 60개월');
                  this.payment_months = 60;
                  this.exp_monthly_payment = Math.ceil(property / this.payment_months);  // 월예상변제금 = 재산 / 60월(변제 개월수)
                  this.total_payment = this.exp_monthly_payment * this.payment_months; // 예상변제금총액 = 월예상변제금 * 60월(변제 개월수)
                  console.log('총 변제금: '+this.total_payment);
                }
    this.exempt_money = total_debt - this.total_payment; //원금탕감액 = 신용채무총액 - 예상변제금총액

    this.exp_monthly_payment = numeral(this.exp_monthly_payment).format('0,0');
    this.payment_months = numeral(this.payment_months).format('0,0');
    this.total_payment = numeral(this.total_payment).format('0,0');
    this.exempt_money = numeral(this.exempt_money).format('0,0');
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  
  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }

}
