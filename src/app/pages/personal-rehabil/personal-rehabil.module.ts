import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PersonalRehabilPage } from './personal-rehabil.page';

const routes: Routes = [
  {
    path: '',
    component: PersonalRehabilPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PersonalRehabilPage]
})
export class PersonalRehabilPageModule {}
