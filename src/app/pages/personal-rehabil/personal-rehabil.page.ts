import { Component, OnInit } from '@angular/core';
import { AlertController, } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

declare var window;

@Component({
  selector: 'app-personal-rehabil',
  templateUrl: './personal-rehabil.page.html',
  styleUrls: ['./personal-rehabil.page.scss'],
})
export class PersonalRehabilPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"

  categories: any;
  category: string;

  advantages: any = [
    { title: "01.",
      description: "채권자 동의 없이 최대 95% 면책가능",
    },
    { title: "02.",
      description: "모든 채무가 조정대상 (사채 및 보증채무 포함)",
    },
    { title: "03.",
      description: "공무원,교사,의사,기업의 임원 자격유지 가능",
    },
    { title: "04.",
      description: "채권자의 독촉 및 추심행위의 중지,금지 가능",
    },
    { title: "05.",
      description: "파산과 달리 개인재산 보유 가능",
    },
    { title: "06.",
      description: "일정금액의 임차보증금 및 6개월간의 생계비 확보",
    },
    { title: "07.",
      description: "파산제도와 달리 낭비,도박 비면책사유가 아니므로 신청가능",
    },
    { title: "08.",
      description: "추후 사정 변경에 따른 변제계획 이라면 변경 수월",
    },
  ]
  workout_cont_bg: any = this.mobile_image_url+"workout_cont_bg.jpg"

  qualifications1: any = [
    { img: this.web_image_url+"re_mean_bg_01.png",
      description: "급여(영업) 소득자",
    },
    { img: this.web_image_url+"re_mean_bg_02.png",
      description: "최저생계비 이상의 소득이 있는 자",
    },
    { img: this.web_image_url+"re_mean_bg_03.png",
      description: "정기적 소득 확인이 가능한자",
    },
    { img: this.web_image_url+"re_mean_bg_04.png",
      description: "재산보다 채무가 많은자",
    },
  ]
  qualifications2: any = [
    { subtitle: "총 담보채무 10억, 무담보채무 5억 이하인 자",
      description: "",
    },
    { subtitle: "고용형태와 상관없이 4대보험에 가입되어있는 정기적인 소득확인이 가능한 자",
      description: "(아르바이트, 비정규직, 일용직 등도 가능)",
    },
    { subtitle: "현재 자신의 재산보다 채무가 많은 자",
      description: "채무보다 재산이 많은 경우, 법원에서 재산을 처분하여 채무를 변제할 수 있다고 판단하기 때문에 제도 이용 불가 (재산평가 = 자신의 재산 + 배우자의 1/2 재산 + 임대차 보증금의 일정한 금액을 공제하여 반영)",
    },
    { subtitle: "급여, 연금, 영업 소득 등 정기적인 소득이 확인이 가능한 자",
      description: "",
    },
  ]

  documents: any = [
    { title: "동사무소",
      description: [
        {desc: "1. 가족관계 증명서(상세)"},
        {desc: "2. 혼인관계증명서(상세)"},
        {desc: "3. 주민등록등본"},
        {desc: "4. 주민등록초본 (주소변동이 나타나는 최근 5년)"},
        {desc: "5. 지방세 세목별 과세증명서 (전 세목 포함, 최근 5년)"},
        {desc: "6. 자동차등록원부 (갑구&을구)"},
        {desc: "7. 출입국사실증명원 (최근5년)-대전,춘천,청주"},
        {desc: "8. 기초생활수급자 증명원"},
        {desc: "9. 장애인 증명원 (해당자만)"},
        {desc: "10. 인감증명서 (채권자 수+5부), 인감도장, 신분증 사본",}
      ]
    },
    { title: "세무서(홈텍스)",
      description: [
        {desc: "1. 소득금액증명원 (최근 3년)"},
        {desc: "2. 사업자등록증"},
        {desc: "3. 폐업사실 확인원"},
        {desc: "4. 자동차등록원부 (갑구&을구)"},
        {desc: "5. 매출,매입처별 세금계산서"},
        {desc: "6. 사실증명원 (사업자유무, 납세증명, 체납,무소득)"}
      ]
    },
    { title: "국민연금공단(FAX가능) - 국번없이 1355",
      description: [
        {desc: "1. 연금정산용 가입내역확인서"},
        {desc: "2. 가입증명서"},
      ]
    },
    { title: "건강보험공단(FAX가능) - 1577-1000",
      description: [
        {desc: "1. 자격득실 확인서 (FAX 가능)"},
        {desc: "2. 최근 1년 부과 내역서(직접 방문)"},
      ]
    },
    { title: "국민연금,건강보험",
      description: [
        {desc: "1. 납입증명원"},
      ]
    },
    { title: "위택스 공인인증서",
      description: [
        {desc: "1. 지방세 납부확인서"},
      ]
    },
    { title: "생명/손해 보험협회 (직접방문 or 공인인증서)",
      description: [
        {desc: "※ 생명/손해 보험협회의 보험가입 확인조회서에 나타난 보험현황 확인 -> 각 보험사에 전화 후 서류발급요청(FAX가능)"},
        {desc: "1. 지방세 납부확인서"},
        {desc: "보험이 유지되고 있다면 ==> 1. 보험납입현황"},
        {desc: "보험이 해지,실효 되었다면 ==> 1. 해지증명서 2. 실효증명서"},
      ]
    },
    { title: "급여소득자 해당",
      description: [
        {desc: "1. 재직증명서"},
        {desc: "2. 급여증명서 (1년)"},
        {desc: "3. 근로소득 원천징수영수증"},
        {desc: "4. 퇴직금예정확인서"},
        {desc: "5. 예상퇴직금 확인서"},
        {desc: "6. 현금수령확인서"},
      ]
    },
    { title: "기타",
      description: [
        {desc: "1. 임대차계약서 사본 (거주지, 사업장)"},
        {desc: "2. 무상거주사실 확인서"},
        {desc: "3. 진술서"},
        {desc: "4. 진단서"},
        {desc: "5. 부과세과표 근거 월평균 소득산출 (사업자 해당)"},
      ]
    },
  ]

  tender_cont_img = this.web_image_url+"tender_cont_img.jpg";
  tender_mean_img = this.web_image_url+"tender_mean_img.jpg"

  style_visual3_05_01 = this.web_image_url+"style_visual3_05_01.jpg"

  constructor(
    public alertCtrl: AlertController,
    private activatedRoute: ActivatedRoute,
    private router: Router,) {
      this.categories= [
        {value: "advantage", name: "장점"}, 
        {value: "qualification", name: "신청자격"}, 
        {value: "document", name: "준비서류"}, 
        {value: "calculator", name: "변제금계산"}
      ];
      this.category = "";
  }

  ngOnInit() {
    //this.category = 'advantage';
    this.activatedRoute.queryParams.subscribe((params) => {
      //console.log("-------->"+params['category']);
      this.category = params['category'];
    });
  }
 
  segmentChanged(ev: any) {
    // console.log('Segment changed event---> ', ev);
    this.category = ev.detail.value;
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
