import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalRehabilPage } from './personal-rehabil.page';

describe('PersonalRehabilPage', () => {
  let component: PersonalRehabilPage;
  let fixture: ComponentFixture<PersonalRehabilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalRehabilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalRehabilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
