import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';

declare var window;

@Component({
  selector: 'app-personal',
  templateUrl: './personal.page.html',
  styleUrls: ['./personal.page.scss'],
})
export class PersonalPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"

  top_image: any = this.web_image_url+"sub_visual03.jpg";

  well_knowns: any = [
    { bk_img: this.web_image_url+"style_tab01.jpg",
      fg_img: this.web_image_url+"web-sub3_icon_01.png",
      segment: "advantage",
      title: "개인회생장점",
    },
    { bk_img: this.web_image_url+"style_tab02.jpg",
      fg_img: this.web_image_url+"web-sub3_icon_02.png",
      segment: "qualification",
      title: "개인회생신청자격",
    },
    { bk_img: this.web_image_url+"style_tab03.jpg",
      fg_img: this.web_image_url+"web-sub3_icon_03.png",
      segment: "document",
      title: "개인회생준비서류",
    },
    { bk_img: this.web_image_url+"style_tab04.jpg",
      fg_img: this.web_image_url+"web-sub3_icon_04.png",
      segment: "calculator",
      title: "변제금계산",
    },
  ]

  constructor(
    public modalCtrl: ModalController, 
    private router: Router,
    public alertCtrl: AlertController,) {
      
    }

  ngOnInit() {
  }

  /* Modal is not working
  async openRehabilitation() {
    console.log('calling ---> openRehabilitation()');
    const modal = await this.modalCtrl.create({
      component: PersonalRehabilPage,
      //componentProps: { passedParam: rehabilitation }
    });
    return await modal.present();
  }
  */

  openRehabilitation(category) {
    //return this.router.navigateByUrl('/personal-rehabil');
    return this.router.navigate(['personal-rehabil'], {queryParams: {category: category}});
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
