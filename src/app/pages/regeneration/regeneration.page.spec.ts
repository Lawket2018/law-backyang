import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegenerationPage } from './regeneration.page';

describe('RegenerationPage', () => {
  let component: RegenerationPage;
  let fixture: ComponentFixture<RegenerationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegenerationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegenerationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
