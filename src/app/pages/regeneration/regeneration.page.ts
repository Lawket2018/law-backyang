import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-regeneration',
  templateUrl: './regeneration.page.html',
  styleUrls: ['./regeneration.page.scss'],
})
export class RegenerationPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/";
  web_image_url: any = "http://newbird15.cafe24.com/images/";
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";

  top_image: any = this.web_image_url+"sub_visual03.jpg";

  categories: any;
  category: string;

  processes: any = [
    { lt: "01. 신청",
      ld1: "변제계획안 제출",
      ld2: "(신청일로부터 14일 이내)",
      rt: "02. 선임",
      rd1: "개인회생위원 선임",
      rd2: null,
    },
    { lt: "03. 금지명령",
      ld1: "추심 및 급여 압류 금지",
      ld2: null,
      rt: "04. 개시결정",
      rd1: "개시결정",
      rd2: "(신청일로부터 3개월 내외)",
    },
    { lt: "05. 집회",
      ld1: "채권자집회",
      ld2: "(개시결정일로부터 3개월 이내)",
      rt: "06. 인가",
      rd1: "변제계획인가",
      rd2: "(신용불량 등록해제)",
    },
    { lt: "07. 면책",
      ld1: "5년 이내 재신청 금지",
      ld2: null,
      rt: null,
      rd1: null,
      rd2: null,
    },
  ]

  tips: any = []; //wr_id, subject, content
  selectFlag: boolean = false;
  tips_index: any;
  queryText: any;

  constructor(
    private http: Http,
    private router: Router,) {
    this.categories= [
      {value: "process", name: "회생절차"}, 
      {value: "tip", name: "TIP"}, 
    ];
    this.category = "process";
   }

  ngOnInit() {
    let init_param = 'getMode=all';
    this.getTips(init_param);
  }

  getTips(param) {
    this.tips = [];
    let queryParams = param;
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.base_url + "by_getTips.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { // 'result'=>true, 'tips'=>$o
      this.tips = data.tips;
      /*
      for (let i = 0; i < Object.keys(data.tips).length; i++) {
        data.tips[i].content = data.tips[i].content.replace(/<\/?[^>]+(>|$)/g, "");
        this.tips.push(data.tips[i]);
      }
      */
    });
  }

  selectedTip(subject, index) {
    //console.log('subject: '+subject+'- index: '+index);
    this.selectFlag = !this.selectFlag;
    this.tips_index = index;
  }

  searchTips() {
    let init_param = 'getMode='+this.queryText;
    this.getTips(init_param);
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
