import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarestepPage } from './carestep.page';

describe('CarestepPage', () => {
  let component: CarestepPage;
  let fixture: ComponentFixture<CarestepPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarestepPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarestepPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
