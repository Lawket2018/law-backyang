import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, } from '@ionic/angular';

declare var window;

@Component({
  selector: 'app-carestep',
  templateUrl: './carestep.page.html',
  styleUrls: ['./carestep.page.scss'],
})
export class CarestepPage implements OnInit {
  
  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"

  visions: any = [
    { img: this.web_image_url+"care_bg01.jpg",
      color: "light",
      step: "STEP 01.",
      title: "전문적인 맞춤 컨설팅 서비스",
      subtitle: "7년의 다양한 경험과 노하우를 기반으로 개인회생 전반에 대한 전문적인 맞춤 컨설팅 서비스를 제공합니다.",
      description: "감당하지 못 할 큰 빚, 막대한 채무해결에 미래가 막막하셨던 분들을 위하여 전문개인회생 팀을 꾸려 개인회생 전반에 대한 전문 컨설팅을 통하여 고객님의 완벽한 채무변제와 행복한 삶을 되찾을 수 있도록 도와드리는것이 백양의 첫번째 희망케어 시스템입니다.",
    },
    { img: this.web_image_url+"care_bg03.jpg",
      color: "light",
      step: "STEP 02.",
      title: "원활한 의사소통",
      subtitle: "고객의 채무를 나의 채무처럼, 원활한 의사소통을 중요시 여겨 1년 365일 항상 고객과의 소통의 문이 열려있습니다.",
      description: "백양법무사 김형백사무는 고객과의 원활한 의사소통이 제일 중요하다고 생각합니다. 무엇이든, 언제든 궁금한 사항이 있으면 전화, 문자, 카톡으로 문의가 가능합니다. 의뢰인의 사건을 나의 사건처럼, 첫 시작부터 최종 면책까지 효율적인 채무조정절차를 위한 의뢰인과의 항상 원활한 커뮤니케이션이 백양의 두번째 희망케어 시스템입니다.",
    },
    { img: this.web_image_url+"care_bg04.jpg",
      color: "light",
      step: "STEP 03.",
      title: "부담없는 수임료 분납가능 서비스",
      subtitle: "고객을 항상 최우선으로 생각하는 백양은 수임료로 인한 부담을 덜어드리기 위해 최대 4회 수임료 분납 서비스가 가능합니다.",
      description: "개인회생 및 파산을 진행하시는 의뢰인분들은 아무래도 경제적으로 어려움을 겪고 있어 수임료 또한 부담스러울 수 있습니다. 백양법무사사무소에서는 실비 ( 인지세, 송달료, 부채증명서 발급비용) 을 투명하게 공개하고 있으며, 수임료 분납이 가능하여 접수비를 포함한 모든 제반 비용을 최대 4개월로 나누어 분납이 가능합니다.",
    },
    { img: this.web_image_url+"care_bg05.jpg",
      color: "light",
      step: "STEP 04.",
      title: "신속하고 철저한 사후관리 서비스",
      subtitle: "백양은 개시 또는 인가결정이 난 이후에도 모든 채무가 없어질 때까지 끝까지 꼼꼼하게 관리해드립니다.",
      description: "개인회생 개시결정 또는 인가가 난 후 문제가 발생하였는데 법무사 또는 변호사 사무실과 연락이 끊겨서 난감한 경우가 비일비재 발생합니다. 백양은 모든 의뢰인이 최종적으로 면책이 되는 날 까지 꼼꼼하게 관리해 드립니다. 회생 도중 급전이 필요해 다른대출을 받아 월 변제금을 감당하기 힘들 때, 소득의 단절이 와서 사건이 폐지 될 위기에 처했을 때, 기타 여러가지 사유로 인가 이후 에 도움을 요청할 때 언제든지 큰 힘이 되어드리는 신속하고 철저한 사후관리 서비스가 백양의 마지막 희망케어시스템입니다.",
    },
  ]

  constructor(
    public alertCtrl: AlertController,
    private router: Router,) {
  }

  ngOnInit() {}

  ionViewDidEnter() {}

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }

}