import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, } from '@ionic/angular';

declare var Swiper: any;
declare var window;

@Component({
  selector: 'app-special',
  templateUrl: './special.page.html',
  styleUrls: ['./special.page.scss'],
})
export class SpecialPage implements OnInit {
  
  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"
  
  top_image: any = this.web_image_url+"sub_visual002.jpg";

  specials: any = [
    { img: this.mobile_image_url+"main_reason_icon_01.png",
      color: "warning",
      title: "첫째, 개인회생 전문가 무료상담",
      subtitle: "개인회생 전문가들이 모여 최상의 맞춤 컨설팅을 제시합니다.", 
      description: "백양법무사 김형백 사무소는 일반사건을 모두 처리하는 다른 법무사 및 변호사와 달리 오로지 개인회생만을 담당하는 전문개인회생팀을 구성하여 의뢰인 한분 한분을 위한 최상의 맞춤 개인회생 컨설팅을 제시하므로 타 법무사 및 변호사 보다 전문성이 뛰어납니다."
    },
    { img: this.mobile_image_url+"main_reason_icon_02.png",
      color: "warning",
      title: "둘째, 개인회생전문팀의 팀워크",
      subtitle: "회생절차도중 분야별 단계별 각각 업무에 특화된 팀을 운용합니다.", 
      description: "개인회생 전문 백양법무사 김형백 사무소는 몇 몇 직원이 개인회생 전 과정을 담당하지 않습니다. 회생도중 분야별 단 계별 각각의 업무에 특화된 팀을 운용하고 있어, 복잡한 사건을 쉽고 빠르게 진행하고 꼼꼼하게 관리하여 최상의 결과 를 이끌어내고 있습니다."
    },
    { img: this.mobile_image_url+"main_reason_icon_03.png",
      color: "warning",
      title: "셋째, 업무효율성 극대화",
      subtitle: "축적된 다년간의 경험을 바탕으로 업무적 효율성을 극대화합니다.", 
      description: "개인회생을 전담으로 사건을 진행해 오면서 백양법무사 김형백 사무소는 축적된 다년간의 경험을 바탕으로 업무 효율성을 극대화하고 있습니다."
    },
    { img: this.mobile_image_url+"main_reason_icon_04.png",
      color: "warning",
      title: "넷째, 안정적인 사건진행",
      subtitle: "개인 회생 자격요건을 충족시키지 못하는 무리한 사건은 수임하지 않습니다.", 
      description: "개인 회생 자격요건을 충족시키지 못하는 의뢰인임에도 불구하고 무리하게 사건을 진행하여 기각이 나는 타 법률 사무소의 사례를 많이 봐왔습니다. 백양법무사 김형백사무소는 사건 진행전부터 개인회생 자격요건을 충족시키는지. 꼼꼼하게 검토하고 진행하므로 기각의 염려가 없습니다."
    },
  ]

  visions: any = [
    { img: this.mobile_image_url+"care_icon_01.jpg",
      color: "light",
      title: "높은 인가율",
      description1: "• 전문개인회생팀 중심으로 지속적인 개인회생 연구",
      description2: "• 노하우 축적으로 높은 인가율 달성",
      description3: "• 신청자격을 면밀히 검토하여 기각률 최소화",
    },
    { img: this.mobile_image_url+"care_icon_02.jpg",
      color: "light",
      title: "신속한 업무진행",
      description1: "• 다년간의 경험을 바탕으로 효율적인 업무진행",
      description2: "• 분야별 업무 세분화로 진행속도 극대화",
      description3: "• 변경되는 회생 제도에 대한 신속한 대응",
    },
    { img: this.mobile_image_url+"care_icon_03.jpg",
      color: "light",
      title: "부담없는 수임료",
      description1: "• 수임료 분납으로 의뢰인 부담 최소화",
      description2: "• 송달료, 인지세는 원칙적으로 실비만 부담",
      description3: "• 기각시 수임료 전액 환불",
    },
  ]

  slideOpts = {
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };

  constructor(
    private router: Router,
    public alertCtrl: AlertController,) {
    }

  ngOnInit() {}

  ionViewDidEnter() {
    var swiper = new Swiper('.swiper-container' , {
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
    });
  }

  openCareStep() {
    return this.router.navigateByUrl('/carestep');
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
