import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import "rxjs/add/operator/map";

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/"
  web_image_url: any = "http://newbird15.cafe24.com/images/"
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";
  bbs_image_url: any = "http://newbird15.cafe24.com/data/file/story/";

  top_image: any = this.web_image_url+"sub_visual05.jpg";

  stories: any = []; // id, subject, content, hit, datetime, bf_file;

  constructor(
    private http: Http,
    private router: Router,) {

     }

  ngOnInit() {
    this.getStories();
  }

  getStories() {
    this.stories = [];
    let queryParams = 'getMode=all';
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.base_url + "by_getStories.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { // 'result'=>true, 'stories'=>$o
      //this.stories = data.stories;
      for (let i = 0; i < Object.keys(data.stories).length; i++) {
        data.stories[i].bf_file = this.bbs_image_url + data.stories[i].bf_file;
        this.stories.push(data.stories[i]);
      }
    });
  }

  openDetailStory(story) {
    return this.router.navigate(['commStory'], {queryParams: {story_content: story.content}});
  }

  searchStories() {
  }

  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }
}
