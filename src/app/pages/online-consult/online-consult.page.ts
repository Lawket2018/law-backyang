import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController, } from '@ionic/angular';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-online-consult',
  templateUrl: './online-consult.page.html',
  styleUrls: ['./online-consult.page.scss'],
})
export class OnlineConsultPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/";
  web_image_url: any = "http://newbird15.cafe24.com/images/";
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";

  top_image: any = this.mobile_image_url+"call_center_bg.jpg";

  phone: string = '';		  // 전화번호
  contents: string = ''; 	// 상담내용
  name: string = ''; 			// 이름

  amountParam: any;
  amount: any;    // 채무금액
  amounts: any = [
    { label: "2000만원 이하", value: "2000만원 이하" },
    { label: "2000만원~3000만원", value: "2000만원~3000만원" },
    { label: "3000만원~4000만원", value: "3000만원~4000만원" },
    { label: "4000만원~5000만원", value: "4000만원~5000만원" },
    { label: "5000만원~6000만원", value: "5000만원~6000만원" },
    { label: "6000만원~7000만원", value: "6000만원~7000만원" },
    { label: "7000만원~8000만원", value: "7000만원~8000만원" },
    { label: "8000만원~9000만원", value: "8000만원~9000만원" },
    { label: "9000만원~1억원", value: "9000만원~1억원" },
    { label: "1억원 이상", value: "1억원 이상" },
  ];

  locationParam: any;
  location: any;  // 거주지역
  locations: any = [
    { label: "서울", value: "서울" },
    { label: "인천", value: "인천" },
    { label: "세종", value: "세종" },
    { label: "대전", value: "대전" },
    { label: "대구", value: "대구" },
    { label: "울산", value: "울산" },
    { label: "광주", value: "광주" },
    { label: "부산", value: "부산" },
    { label: "제주", value: "제주" },
    { label: "강원도", value: "강원도" },
    { label: "경기도", value: "경기도" },
    { label: "충청북도", value: "충청북도" },
    { label: "충청남도", value: "충청남도" },
    { label: "경상북도", value: "경상북도" },
    { label: "경상남도", value: "경상남도" },
    { label: "전라북도", value: "전라북도" },
    { label: "전라남도", value: "전라남도" },
  ];

  isAgreed: boolean = false;
  username: any;

  constructor(
    private http: Http,
    public alertCtrl: AlertController,) { 
  }

  ngOnInit() {

  }

  selectedAmount(e) {
    console.log('selected Court --------> ' +this.amount);
    //this.courtParam = '&court=' +this.court;
    //this.getSuccessCase();
  }

  selectedLocation(e) {
    console.log('selected decision --------> ' +this.location);
    //this.decisionParam = '&decision=' +this.decision;
    //this.getSuccessCase();
  }

  submitRequest() {
    if(!this.isAgreed) {
      let alertContent = {
        subject: "알림",
        content: "개인정보처리방침안내의 내용에 동의해야 합니다."
      }
      this.popupAlert(alertContent)
    } else {
      console.log('this.name ===> '+this.name);
      console.log('this.phone ===> '+this.phone);
      console.log('this.amount ===> '+this.amount);
      console.log('this.location ===> '+this.location);
      if(this.name == undefined || this.name == '') {
        let alertContent = {
          subject: "알림",
          content: "이름을 입력해 주세요."
        }
        this.popupAlert(alertContent)
      } else if(this.phone == undefined || this.phone == '') {
        let alertContent = {
          subject: "알림",
          content: "연락 가능한 전화번호를 입력해야 합니다."
        }
        this.popupAlert(alertContent)
      } else if(this.amount == undefined || this.amount == '') {
        let alertContent = {
          subject: "알림",
          content: "채무금액을 선택해 주세요."
        }
        this.popupAlert(alertContent)
      } else if(this.location == undefined || this.location == '') {
        let alertContent = {
          subject: "알림",
          content: "거주지역을 선택해 주세요."
        }
        this.popupAlert(alertContent)
      } else if(this.contents == undefined || this.contents == '') {
        let alertContent = {
          subject: "알림",
          content: "상담하실 내용을 입력해주세요."
        }
        this.popupAlert(alertContent);
      } else {
        this.postOnlineConsult();
      }
    }
  }
  
  /* content Alert  */
  async popupAlert(alertContent) {
    const alert = await this.alertCtrl.create({
      header: alertContent.subject,
      message: alertContent.content,
      buttons: ['닫기']
    });
    await alert.present();
  }

  postOnlineConsult() {
    let queryParams = 'phone=' +this.phone+ 
                      '&contents=' +this.contents+ 
                      '&name=' +this.name+ 
                      '&amount=' +this.amount+ 
                      '&location=' +this.location;
    let body   : string	 = queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.base_url + "by_CRUDOnlineConsult.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
      if (!data.result) {
        let alertContent = {
          subject: "ERROR!",
          content: data.msg
        }
        this.popupAlert(alertContent)
      } else {
        console.log('===> '+data.msg);
        let alertContent = {
          subject: "신청완료",
          content: "온라인 상담신청이 완료되었습니다."
        }
        this.popupAlert(alertContent);
      }
    });
  }

}
