import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { AlertController, } from '@ionic/angular';
import "rxjs/add/operator/map";

declare var window;

@Component({
  selector: 'app-review',
  templateUrl: './review.page.html',
  styleUrls: ['./review.page.scss'],
})
export class ReviewPage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/";
  web_image_url: any = "http://newbird15.cafe24.com/images/";
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";

  top_image: any = this.web_image_url+"sub_visual06.jpg";

  categories: any;
  category: string;

  noticeParam: any;
  notices: any = [];

  epilogueParam: any;
  epilogues: any = [];
  last_num: any;
  doInfiniteFlag: boolean = true;

  constructor(
    private http: Http,
    public alertCtrl: AlertController, 
    private router: Router,) { 
      this.categories= [
        {value: "전체", name: "전체"}, 
        {value: "개인회생", name: "개인회생"}, 
        {value: "개인파산", name: "개인파산"}, 
        {value: "글쓰기", name: "글쓰기"},
      ];
      this.category = "전체";
    }

  ngOnInit() {}

  ionViewDidEnter() {
    this.noticeParam = 'getMode=all';
    this.getNotices();
    this.epilogueParam = 'getMode=전체';
    this.getEpilogues();
    this.category = "전체";
  }

  segmentChanged(ev: any) {
    //console.log('Clicked Segment =====>', this.category);
    if(this.category == '글쓰기') {
      return this.router.navigateByUrl('/review-write');
    } else {
      this.epilogueParam = 'getMode='+this.category;
      this.doInfiniteFlag = true;
      this.getEpilogues();
    }
  }

  getNotices() {
    this.notices = [];
    let queryParams =  this.noticeParam;
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.base_url + "by_getNotices.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { // 'result'=>true, 'notices'=>$o
      // this.notices = data.notices;
      for (let i = 0; i < Object.keys(data.notices).length; i++) {
        data.notices[i].content = data.notices[i].content.replace(/<\/?[^>]+(>|$)/g, "");
        this.notices.push(data.notices[i]);
      }
    });
  }

  getEpilogues() {
    this.epilogues = [];
    let queryParams =  this.epilogueParam;
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.base_url + "by_getEpilogues.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { // 'result'=>true, 'epilogues'=>$o
      // this.epilogues = data.epilogues;
      for (let i = 0; i < Object.keys(data.epilogues).length; i++) {
        data.epilogues[i].content = data.epilogues[i].content.replace(/<\/?[^>]+(>|$)/g, "");
        this.epilogues.push(data.epilogues[i]);
      }
      this.last_num = data.last_num;
      console.log('epilogues last_num -----> '+this.last_num);
      if( Object.keys(data.epilogues).length < 20) {
        this.doInfiniteFlag = false;
      }
    });
  }

  async popupAlert(alertContent) {
    const alert = await this.alertCtrl.create({
      header: alertContent.subject,
      message: alertContent.content,
      buttons: ['닫기']
    });
    await alert.present();
  }

  loadData(event) {
    if(this.doInfiniteFlag == true) {
      let infiniteParam =  '&last_num='+this.last_num;
      let queryParams =  this.epilogueParam + infiniteParam;
      let body   : string	 = queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.base_url + "by_getEpilogues.php";

      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != true) {
          this.doInfiniteFlag = false;
        } else {
          for (let i = 0; i < Object.keys(data.epilogues).length; i++) {
            data.epilogues[i].content = data.epilogues[i].content.replace(/<\/?[^>]+(>|$)/g, "");
            this.epilogues.push(data.epilogues[i]);
          }
          this.last_num = data.last_num;
          console.log('epilogues last_num -----> '+this.last_num);
          if( Object.keys(data.epilogues).length < 20) {
            this.doInfiniteFlag = false;
          }
        }
        // console.log('Next operation has ended');
        event.target.complete();
      });
    }
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  
  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }

}
