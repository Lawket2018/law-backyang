import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
import "rxjs/add/operator/map";

declare var window;

@Component({
  selector: 'app-review-write',
  templateUrl: './review-write.page.html',
  styleUrls: ['./review-write.page.scss'],
})
export class ReviewWritePage implements OnInit {

  mobile_image_url: any = "http://newbird15.cafe24.com/mobile/images/";
  web_image_url: any = "http://newbird15.cafe24.com/images/";
  base_url: any = "http://newbird15.cafe24.com/data/app_php/";

  top_image: any = this.mobile_image_url+"call_center_bg.jpg";

  /* create epilogue */
  name: string = ''
  password: string = ''
  subject: string = ''
  content: string = ''
  ca_name: any;  // 분류
  ca_names: any = [
    { label: "개인회생", value: "개인회생" },
    { label: "개인파산", value: "개인파산" },
  ];

  constructor(
    private http: Http,
    public alertCtrl: AlertController,
    private router: Router,) { }

  ngOnInit() {
  }

  selectedCaname() {
    console.log('selected Caname --------> ' +this.ca_name);
  }
  
  submit() {
    // name, password, subject, content, ca_name
    console.log('this.name ===> '+this.name);
    console.log('this.password ===> '+this.password);
    console.log('this.subject ===> '+this.subject);
    console.log('this.content ===> '+this.content);
    console.log('this.ca_name ===> '+this.ca_name);
    if(this.name == undefined || this.name == '') {
      let alertContent = {
        subject: "알림",
        content: "이름을 입력해 주세요."
      }
      this.popupAlert(alertContent);
    } else if(this.password == undefined || this.password == '') {
      let alertContent = {
        subject: "알림",
        content: "비밀번호를 입력해 주세요."
      }
      this.popupAlert(alertContent);
    } else if(this.ca_name == undefined || this.ca_name == '') {
      let alertContent = {
        subject: "알림",
        content: "분류 선택해 주세요."
      }
      this.popupAlert(alertContent);
    } else if(this.subject == undefined || this.subject == '') {
      let alertContent = {
        subject: "알림",
        content: "제목을 입력해 주세요."
      }
      this.popupAlert(alertContent);
    } else if(this.content == undefined || this.content == '') {
      let alertContent = {
        subject: "알림",
        content: "내용을 입력해 주세요."
      }
      this.popupAlert(alertContent);
    } else {
      let chkPwdResualt = chkPwd(this.password);
      if(chkPwdResualt != 'good'){
        let alertContent = {
        subject: "알림",
        content: chkPwdResualt
      }
      this.popupAlert(alertContent);
      } else {
        this.postReviewWrite();
      }
    }

    function chkPwd(str){
      let pw = str;
      let num = pw.search(/[0-9]/g);
      let eng = pw.search(/[a-z]/ig);
      //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

      if(pw.length < 6 || pw.length > 15){
        return '비밀번호는 영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
      }
      if(pw.search(/₩s/) != -1){
        return '비밀번호는 공백없이 입력해주세요.';
      } 
      if(num < 0 || eng < 0){
        return '비밀번호는 영문.숫자를 혼합하여 입력해주세요.';
      }
      return 'good';
    };

  }
  
  /* content Alert  */
  async popupAlert(alertContent) {
    const alert = await this.alertCtrl.create({
      header: alertContent.subject,
      message: alertContent.content,
      buttons: ['닫기']
    });
    await alert.present();
  }

  postReviewWrite() {
    // name, password, subject, content, ca_name
    console.log('<===== postReviewWrite ===> ');

    let queryParams = 'name=' +this.name+ 
                      '&password=' +this.password+ 
                      '&subject=' +this.subject+ 
                      '&content=' +this.content+ 
                      '&ca_name=' +this.ca_name;
    let body   : string	 = queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.base_url + "by_CRUDEpilogue.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
      if (!data.result) {
        let alertContent = {
          subject: "ERROR!",
          content: data.msg
        }
        this.popupAlert(alertContent)
      } else {
        console.log('===> '+data.msg);
      }
    });
  }

  async openDialPopup() {
    const alert = await this.alertCtrl.create({
      header: '전화걸기',
      message: '<strong>상담시간:24시간</strong><br><br>연중무휴/휴일•주말에도 상담 가능합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '선택',
          handler: () => {
            window.location = 'tel:1566-2320';
            //console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  
  openOnlineConsult() {
    return this.router.navigateByUrl('/online-consult');
  }

}
