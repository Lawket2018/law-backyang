import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewWritePage } from './review-write.page';

describe('ReviewWritePage', () => {
  let component: ReviewWritePage;
  let fixture: ComponentFixture<ReviewWritePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewWritePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewWritePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
