import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comm-story',
  templateUrl: './comm-story.page.html',
  styleUrls: ['./comm-story.page.scss'],
})
export class CommStoryPage implements OnInit {

  story: any;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      console.log("-------->"+params['story_content']);
      this.story = params['story_content'];
    });
  }

}
