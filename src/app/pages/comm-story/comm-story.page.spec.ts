import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommStoryPage } from './comm-story.page';

describe('CommStoryPage', () => {
  let component: CommStoryPage;
  let fixture: ComponentFixture<CommStoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommStoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommStoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
